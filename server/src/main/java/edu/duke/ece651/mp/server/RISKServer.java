package edu.duke.ece651.mp.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.lang.reflect.Type;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.duke.ece651.mp.common.OrderParameter;
import edu.duke.ece651.mp.common.Player;

/**
 * This class handles the connection between server and client (on the server
 * side)
 */
public class RISKServer {

    /**
     * The ServerSocket for this game
     */
    final ServerSocket ss;

    private Socket client;
    private InputStream fromClient1;
    private InputStream fromClient2;
    private Socket client1;
    private Socket client2;
    private Player player1;
    private Player player2;
    public RISKServer(int port) throws IOException {
        this.ss = new ServerSocket(port);
        this.client = null;
    }

    /**
     * accept a client socket connection, regieter the connection to the field
     */
    public void acceptClient() throws IOException {
        this.client1 = ss.accept();
        this.fromClient1 = client1.getInputStream();
        player1 = new Player(0,"Player1");

        this.client2 = ss.accept();
        this.fromClient2 =client2.getInputStream();
        player2 = new Player(1,"Player2");
    }

    /**
     * Send an object as JSON to the client
     *
     * @param obj is an Object to be sent
     */
    public void sendJSON(Object obj) throws IOException {
        Gson gson = new Gson();
        String toSend = gson.toJson(obj);
        OutputStream out1 = client1.getOutputStream();
        ObjectOutputStream o1 = new ObjectOutputStream(out1);
        o1.writeObject(toSend);
        out1.flush();

        OutputStream out2 = client2.getOutputStream();
        ObjectOutputStream o2 = new ObjectOutputStream(out2);
        o2.writeObject(toSend);
        out2.flush();
    }

    /**
     * Server thread that waits for user orders.
     * TODO: rule checkers and return result to client
     */
    private class RISKTurnThread extends Thread {
        private ArrayList<OrderParameter> orderParams;
        /** the list of received orders */
        ObjectInputStream objIn;

        /** the socket to client */

        /**
         * Constructor
         *
         * @param orderParams
         * @param in
         */
        public RISKTurnThread(ArrayList<OrderParameter> orderParams, InputStream in) throws IOException {
            this.orderParams = orderParams;
            this.objIn = new ObjectInputStream(in);
        }

        /**
         * The main function of this thread
         */
        public void run() {
            OrderParameter orderParam = null;
            do {
                try {
                    orderParam = receiveOrderParameter();
                    orderParams.add(orderParam);
                } catch (Exception e) {
                }
            } while (!orderParam.getOrderType().equals("commit"));
        }

        /**
         * receives one OrderParameter from the client
         *
         * @return OrderParameter the received OrderParameter
         * @throws IOException
         */
        private OrderParameter receiveOrderParameter() throws IOException {
            String line = null;
            try {
                line = (String) objIn.readObject();
            } catch (Exception e) {
            }
            Gson gson = new Gson();
            Type type = new TypeToken<OrderParameter>() {
            }.getType();
            OrderParameter orderParam = gson.fromJson(line, type);
            return orderParam;
        }
    }

    public ArrayList<OrderParameter> recieveOrderPhase() throws IOException {
        ArrayList<OrderParameter> orders = new ArrayList<OrderParameter>();
        RISKTurnThread p1 = new RISKTurnThread(orders, fromClient1);
        RISKTurnThread p2 = new RISKTurnThread(orders, fromClient2);
        p1.start();
        p2.start();
        try {
            p1.join();
            p2.join();
        } catch (InterruptedException e) {
        }
        return orders;
    }
}
