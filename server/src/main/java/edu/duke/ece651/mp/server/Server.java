package edu.duke.ece651.mp.server;

import java.io.IOException;
import java.util.ArrayList;

import edu.duke.ece651.mp.common.MapFactory;
import edu.duke.ece651.mp.common.Territory;
import edu.duke.ece651.mp.common.WorldMap;

public class Server {

  final WorldMap map;

  public Server(WorldMap map){
    this.map = map;
  }
  
  public static void main(String[] args) throws IOException {
    RISKServer riskServer = new RISKServer(7777);
    riskServer.acceptClient();
    Territory test = new Territory("test",0);
    ArrayList<Territory> territories = new ArrayList<Territory>();
    territories.add(test);
    //WorldMap map = new WorldMap(territories);
    MapFactory factory = new MapFactory();
    WorldMap map = factory.createBasicMap();
    Server server = new Server(map);
    riskServer.sendJSON(map);
  }
}













