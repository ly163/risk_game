package edu.duke.ece651.mp.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;

import com.google.gson.Gson;

import org.junit.jupiter.api.Test;

public class RISKServerTest {

  private void receiveJSON(ByteArrayOutputStream bytes) throws Exception{
    PrintStream out = new PrintStream(bytes, true);
    Socket server = new Socket("127.0.0.1", 6666);
    InputStream in = server.getInputStream();
    ObjectInputStream i = new ObjectInputStream(in);
    String line = null;
    try{
      line = (String) i.readObject();
    }
    catch(Exception e){
    }
    Gson gson = new Gson();
    String str = gson.fromJson(line, String.class);
    out.print(str);
  }
  
  @Test
  public void test_sendJSON() throws Exception {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    RISKServer server = new RISKServer(6666);
    Thread th = new Thread() {
        @Override()
        public void run() {
          try {
            sleep(1000);
            Thread c1 = new Thread(()-> {
              try {
                receiveJSON(bytes);
              } catch (Exception e) {
                e.printStackTrace();
              }
            });
            Thread c2 = new Thread(()-> {
              try {
                receiveJSON(new ByteArrayOutputStream());
              } catch (Exception e) {
                e.printStackTrace();
              }
            });
            c1.start();
            c2.start();
            c1.join();
            c2.join();
          } catch (Exception e) {
          }
        }
      };
    th.start();
    server.acceptClient();
    server.sendJSON("test");
    th.join();
    assertEquals("test", bytes.toString());
  }

}
