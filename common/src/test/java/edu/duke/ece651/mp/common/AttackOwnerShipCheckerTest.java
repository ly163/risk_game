package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AttackOwnerShipCheckerTest {

    @Test
    public void testForAttackOwnerShipChecker(){
        WorldMap map = new MapFactory().createBasicMap();
        AttackOwnerShipChecker checker = new AttackOwnerShipChecker();
        assertFalse(checker.check(new AttackOrder(1,2,1),map));
        assertFalse(checker.check(new AttackOrder(4,5,1),map));
    }
}
