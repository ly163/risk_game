package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MoveOrderTest {
  @Test
  public void test_MoveOrder() {
    MapFactory factory = new MapFactory();
    WorldMap map = factory.createBasicMap();
    MoveOrder move = new MoveOrder(0, 1, 3);
    move.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(13, map.findTerritory(1).getUnits());
    move.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(13, map.findTerritory(1).getUnits());
    move.revoke();
    assertEquals(10, map.findTerritory(0).getUnits());
    assertEquals(10, map.findTerritory(1).getUnits());
  }
}
