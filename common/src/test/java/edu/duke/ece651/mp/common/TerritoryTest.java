package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TerritoryTest {
  @Test
  public void test_getters() {
    Territory DUT = new Territory("Test", 0);
    assertEquals("Test", DUT.getName());
    assertEquals(0, DUT.getId());
    assertEquals(0, DUT.getUnits());
    assertEquals(0, DUT.getEnemyUnits());
  }

  @Test
  public void test_setters() {
    Territory DUT = new Territory("Test", 0);
    DUT.setUnits(1);
    DUT.setEnemyUnits(2);
    assertEquals(1, DUT.getUnits());
    assertEquals(2, DUT.getEnemyUnits());
  }

  @Test
  public void test_combatSimulation(){
    Territory test = new Territory("test", 0);
    test.setUnits(20);
    test.setEnemyUnits(10);
    Dice dice = new Dice_20(1);
    Player p1 = new Player(0, "p1");
    Player p2 = new Player(1, "p2");
    p1.addTerritory(test);
    CombatResult r1 = test.combatSimulation(p2, p1, dice);
    assertEquals(1, r1.getAttackerId());
    assertEquals(0, r1.getDefenderId());
    assertEquals(0, r1.getTerritoryId());
    assertFalse(r1.getSucceed());
    assertEquals(8, test.getUnits());
    assertEquals(0, test.getEnemyUnits());
    assertTrue(p1.checkOwnership(test));
    assertFalse(p2.checkOwnership(test));
    test.setEnemyUnits(20);
    CombatResult r2 = test.combatSimulation(p2, p1, dice);
    assertEquals(1, r2.getAttackerId());
    assertEquals(0, r2.getDefenderId());
    assertEquals(0, r2.getTerritoryId());
    assertTrue(r2.getSucceed());
    assertEquals(11, test.getUnits());
    assertEquals(0, test.getEnemyUnits());
    assertFalse(p1.checkOwnership(test));
    assertTrue(p2.checkOwnership(test));
    assertEquals(null, test.combatSimulation(p2, p1, dice));
  }
  
  @Test
  public void test_hashCode() {
    Territory DUT = new Territory("Foo", 0);
    Territory DUT00 = new Territory("Foo", 0);
    Territory DUT01 = new Territory("Foo", 1);
    Territory DUT10 = new Territory(null, 0);
    assertEquals(DUT.hashCode(), DUT00.hashCode());
    assertNotEquals(DUT.hashCode(), DUT01.hashCode());
    assertNotEquals(DUT.hashCode(), DUT10.hashCode());
    DUT00.setUnits(7);
    DUT00.setEnemyUnits(42);
    assertEquals(DUT.hashCode(), DUT00.hashCode());
  }
}
