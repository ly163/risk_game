package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MovePathCheckerTest {
    @Test
    public void testMovePathChecker() {
        WorldMap map = new MapFactory().createBasicMap();
        RuleChecker checker = new MovePathChecker();
        assertTrue(checker.check(new MoveOrder(1, 2, 1),map));
        assertFalse(checker.check(new MoveOrder(1, 5, 1),map));
    }
}
