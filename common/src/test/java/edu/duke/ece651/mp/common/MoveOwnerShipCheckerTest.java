package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoveOwnerShipCheckerTest {
    @Test
    public void testMoveOwnerShipChecker() {
        WorldMap map = new MapFactory().createBasicMap();
        RuleChecker checker = new MoveOwnerShipChecker();
        assertTrue(checker.check(new MoveOrder(1, 2, 1),map));
        assertFalse(checker.check(new MoveOrder(1, 5, 1),map));
    }

}
