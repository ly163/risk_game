package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AttackAdjacentCheckerTest {
    @Test
    public void testAdj() {
        WorldMap map = new MapFactory().createBasicMap();
        RuleChecker checker = new AttackAdjacentChecker();
        assertTrue(checker.check(new AttackOrder(1, 4, 1),map));
        assertFalse(checker.check(new AttackOrder(1, 5, 1),map));
    }

}
