package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

public class WorldMapTest {
  @Test
  public void test_map() {
    MapFactory f = new MapFactory();
    WorldMap map = f.createBasicMap();
    ArrayList<Player> players = map.getPlayers();
    Iterator<Player> it = players.iterator();
    Player p1 = it.next();
    Player p2 = it.next();
    for(Territory t : p1.getTerritories()){
      assertTrue(map.checkOwnership(p1, t));
      assertFalse(map.checkOwnership(p2, t));
      for(Territory t2 : p1.getTerritories()){
        assertTrue(map.isConnected(t, t2, p1));
        assertEquals(map.getNeighbors(t).contains(t2), map.isNeighbor(t, t2));
      }
      for(Territory t2 : p2.getTerritories()){
        assertFalse(map.isConnected(t, t2, p1));
        assertEquals(map.getNeighbors(t).contains(t2), map.isNeighbor(t, t2));
      }
    }
    for(Territory t : p2.getTerritories()){
      assertTrue(map.checkOwnership(p2, t));
      assertFalse(map.checkOwnership(p1, t));
      for(Territory t2 : p2.getTerritories()){
        assertTrue(map.isConnected(t, t2, p2));
        assertEquals(map.getNeighbors(t).contains(t2), map.isNeighbor(t, t2));
      }
      for(Territory t2 : p1.getTerritories()){
        assertFalse(map.isConnected(t, t2, p2));
        assertEquals(map.getNeighbors(t).contains(t2), map.isNeighbor(t, t2));
      }
    }
    assertEquals(6, map.getTerritories().size());

    assertEquals("t1", map.findTerritory(0).getName());
    assertEquals(null, map.findTerritory(7));
  }

  @Test
  public void test_getOwner(){
    MapFactory f = new MapFactory();
    WorldMap map = f.createBasicMap();
    for(Player p : map.getPlayers()){
      for(Territory t : p.getTerritories()){
        assertEquals(p, map.getOwner(t));
      }
    }
    Territory test = new Territory("Test", 9);
    assertEquals(null, map.getOwner(test));
  }

  @Test
  public void test_combatSimulation(){
    MapFactory f = new MapFactory();
    WorldMap map = f.createBasicMap();
    Dice dice = new Dice_20(1);
    Territory t1 = map.findTerritory(0);
    t1.setUnits(20);
    t1.setEnemyUnits(10);
    Territory t2 = map.findTerritory(5);
    t2.setUnits(8);
    t2.setEnemyUnits(20);
    ArrayList<CombatResult> results = map.combatSimulation(dice);
    assertEquals(2, results.size());
    CombatResult r2 = results.get(0);
    CombatResult r1 = results.get(1);
    assertEquals(0, r1.getAttackerId());
    assertEquals(1, r1.getDefenderId());
    assertEquals(5, r1.getTerritoryId());
    assertTrue(r1.getSucceed());
    assertEquals(1, r2.getAttackerId());
    assertEquals(0, r2.getDefenderId());
    assertEquals(0, r2.getTerritoryId());
    assertFalse(r2.getSucceed());
    Player p1 = map.getPlayers().get(0);
    Player p2 = map.getPlayers().get(1);
    assertEquals(4, p1.getTerritories().size());
    assertEquals(2, p2.getTerritories().size());
  }
   
  
}
