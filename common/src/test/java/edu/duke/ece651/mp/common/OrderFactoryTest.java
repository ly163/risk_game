package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class OrderFactoryTest {
  @Test
  public void test_OrderFactory() {
    MapFactory factory = new MapFactory();
    WorldMap map = factory.createBasicMap();
    OrderParameter p1 = new OrderParameter(0, 1, 3, "Move");
    OrderParameter p2 = new OrderParameter(0, 1, 3, "Attack");
    OrderParameter p3 = new OrderParameter(0, 1, 3, "Test");
    OrderFactory of = new OrderFactory();
    Order move = of.createOrder(p1);
    Order attack = of.createOrder(p2);
    assertEquals(null, of.createOrder(p3));
    move.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(13, map.findTerritory(1).getUnits());
    move.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(13, map.findTerritory(1).getUnits());
    move.revoke();
    assertEquals(10, map.findTerritory(0).getUnits());
    assertEquals(10, map.findTerritory(1).getUnits());
    attack.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(3, map.findTerritory(1).getEnemyUnits());
    attack.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(3, map.findTerritory(1).getEnemyUnits());
    attack.revoke();
    assertEquals(10, map.findTerritory(0).getUnits());
    assertEquals(0, map.findTerritory(1).getEnemyUnits());
  }

}
