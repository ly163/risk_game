package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class OrderArgumentCheckerTest {
    @Test
    public void testOrderArgumentChecker() {
        WorldMap map = new MapFactory().createBasicMap();
        RuleChecker checker = new OrderArgumentChecker();
        assertFalse(checker.check(new MoveOrder(100,1,1),map));
        assertTrue(checker.check(new MoveOrder(1,1,1),map));
        assertTrue(checker.check(new AttackOrder(1,1,1),map));
        assertFalse(checker.check(null,map));
        assertFalse(checker.check(new MoveOrder(1,2,-1),map));
        assertFalse(checker.check(new MoveOrder(1,2,-1),null));
    }
}
