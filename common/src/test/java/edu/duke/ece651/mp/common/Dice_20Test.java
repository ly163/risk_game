package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class Dice_20Test {
  @Test
  public void test_roll() {
    Random test = new Random(1);
    Dice_20 dice = new Dice_20(1);
    assertEquals(test.nextInt(20)+1, dice.roll());
    assertEquals(test.nextInt(20)+1, dice.roll());
  }

}
