package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AttackOrderTest {
  @Test
  public void test_AttackOrder() {
    MapFactory factory = new MapFactory();
    WorldMap map = factory.createBasicMap();
    AttackOrder attack = new AttackOrder(0, 1, 3);
    attack.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(3, map.findTerritory(1).getEnemyUnits());
    attack.execute(map);
    assertEquals(7, map.findTerritory(0).getUnits());
    assertEquals(3, map.findTerritory(1).getEnemyUnits());
    attack.revoke();
    assertEquals(10, map.findTerritory(0).getUnits());
    assertEquals(0, map.findTerritory(1).getEnemyUnits());
  }

}
