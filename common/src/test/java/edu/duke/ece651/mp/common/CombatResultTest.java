package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class CombatResultTest {
  @Test
  public void test_CombatResult() {
    CombatResult test = new CombatResult(0, 1, 0, true);
    assertEquals(0, test.getAttackerId());
    assertEquals(1, test.getDefenderId());
    assertEquals(0, test.getTerritoryId());
    assertEquals(true, test.getSucceed());
  }

}
