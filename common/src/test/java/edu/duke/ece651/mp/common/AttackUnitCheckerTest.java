package edu.duke.ece651.mp.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AttackUnitCheckerTest {
    @Test
    public void testForAttackUnitChecker() {
        WorldMap map = new MapFactory().createBasicMap();
        AttackUnitChecker checker = new AttackUnitChecker();
        assertFalse(checker.check(new AttackOrder(1,1,0),map));
        assertFalse(checker.check(new AttackOrder(1,1,-1),map));
        assertTrue(checker.check(new AttackOrder(1,1,1),map));
    }

}
