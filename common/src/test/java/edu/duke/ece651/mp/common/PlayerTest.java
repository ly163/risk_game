package edu.duke.ece651.mp.common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlayerTest {
  @Test
  public void test_Player() {
    Territory t1 = new Territory("t1", 0);
    Territory t2 = new Territory("t2", 1);
    Player p1 = new Player(1, "p1");
    p1.addTerritory(t1);
    assertTrue(p1.checkOwnership(t1));
    assertFalse(p1.checkOwnership(t2));
    assertEquals(1, p1.getTerritories().size());
    p1.removeTerritory(t1);
    assertFalse(p1.checkOwnership(t1));
    assertEquals(1, p1.getId());
    assertEquals("p1", p1.getName());
  assertEquals(0, p1.getTerritories().size());
  }

}
