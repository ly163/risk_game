package edu.duke.ece651.mp.common;

public class MovePathChecker implements RuleChecker {
    Class<MoveOrder> targetClass;

    public MovePathChecker() {
        this.targetClass = MoveOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        MoveOrder moveOrder = (MoveOrder) order;
        Territory start = worldMap.findTerritory(moveOrder.startId);
        Player owner = worldMap.getOwner(start);
        return worldMap.isConnected(start,
                worldMap.findTerritory(moveOrder.destId),owner);
    }
}
