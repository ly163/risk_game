package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/** This class creats WorldMap object with preset layouts
 */
public class MapFactory {

  /** This method creats a Basic map we can use 
   * @return a WorldMap object
   */
  public WorldMap createBasicMap(){
    Territory t1 = new Territory("t1", 0);
    Territory t2 = new Territory("t2", 1);
    Territory t3 = new Territory("t3", 2);
    Territory t4 = new Territory("t4", 3);
    Territory t5 = new Territory("t5", 4);
    Territory t6 = new Territory("t6", 5);
    
    ArrayList<Territory> a1 = new ArrayList<Territory>();
    ArrayList<Territory> a2 = new ArrayList<Territory>();
    ArrayList<Territory> a3 = new ArrayList<Territory>();
    ArrayList<Territory> a4 = new ArrayList<Territory>();
    ArrayList<Territory> a5 = new ArrayList<Territory>();
    ArrayList<Territory> a6 = new ArrayList<Territory>();
    
    a1.add(t2);
    a1.add(t3);
    
    a2.add(t1);
    a2.add(t3);
    a2.add(t4);
    a2.add(t5);
    
    a3.add(t1);
    a3.add(t2);
    a3.add(t4);
    a3.add(t5);

    a4.add(t6);
    a4.add(t5);
    a4.add(t2);
    a4.add(t3);

    a5.add(t6);
    a5.add(t4);
    a5.add(t2);
    a5.add(t3);

    a6.add(t4);
    a6.add(t5);

    ArrayList<Territory> territories = new ArrayList<Territory>();
    territories.add(t1);
    territories.add(t2);
    territories.add(t3);
    territories.add(t4);
    territories.add(t5);
    territories.add(t6);

    for(Territory t : territories){
      t.setUnits(10);
    }
    
    HashMap<Territory, ArrayList<Territory>> adjacencies = new HashMap<Territory, ArrayList<Territory>>();
    adjacencies.put(t1, a1);
    adjacencies.put(t2, a2);
    adjacencies.put(t3, a3);
    adjacencies.put(t4, a4);
    adjacencies.put(t5, a5);
    adjacencies.put(t6, a6);

    Player p1 = new Player(0, "Red");
    Player p2 = new Player(1, "Blue");
    p1.addTerritory(t1);
    p1.addTerritory(t2);
    p1.addTerritory(t3);
    p2.addTerritory(t4);
    p2.addTerritory(t5);
    p2.addTerritory(t6);
    ArrayList<Player> players = new ArrayList<Player>();
    players.add(p1);
    players.add(p2);

    WorldMap map = new WorldMap(territories, adjacencies, players);
    return map;
  }
}
