package edu.duke.ece651.mp.common;

public class AttackOwnerShipChecker implements RuleChecker{
    Class<AttackOrder> targetClass;

    public AttackOwnerShipChecker() {
        this.targetClass = AttackOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        AttackOrder moveOrder = (AttackOrder) order;
        Player ownerStart = worldMap.getOwner(worldMap.findTerritory(moveOrder.startId));
        Player ownerEnd = worldMap.getOwner(worldMap.findTerritory(moveOrder.destId));
        return !ownerStart.equals(ownerEnd);
    }
}
