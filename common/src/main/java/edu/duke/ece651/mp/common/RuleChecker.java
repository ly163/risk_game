package edu.duke.ece651.mp.common;

public interface RuleChecker {
    boolean check(Order order,WorldMap map);
}
