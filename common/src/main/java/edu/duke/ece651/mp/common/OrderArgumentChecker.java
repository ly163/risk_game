package edu.duke.ece651.mp.common;

import java.lang.reflect.Field;

public class OrderArgumentChecker implements RuleChecker {


    @Override
    public boolean check(Order order, WorldMap worldMap) {
        if (order == null || worldMap==null) {
            return false;
        }
        Class<? extends Order> orderClass = order.getClass();
        try {
            for (Field field : orderClass.getDeclaredFields()) {
                if (field.getName().equals("startId") || field.getName().equals("destId")) {
                    int id = field.getInt(order);
                    if (worldMap.findTerritory(id) == null) {
                        return false;
                    }
                } else if (field.getName().equals("units")) {
                    if (field.getInt(order) < 0) {
                        return false;
                    }
                }
            }
            return true;
        } catch (IllegalAccessException e) {
            return false;
        }
    }
}
