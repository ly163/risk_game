package edu.duke.ece651.mp.common;

public class MoveOwnerShipChecker implements RuleChecker {
    Class<MoveOrder> targetClass;

    public MoveOwnerShipChecker() {
        this.targetClass = MoveOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        MoveOrder moveOrder = (MoveOrder) order;
        Territory start = worldMap.findTerritory(moveOrder.startId);
        Territory end = worldMap.findTerritory(moveOrder.destId);
        if (!worldMap.getOwner(start).
                equals(worldMap.getOwner(end))) {
            return false;
        }
        return true;
    }
}
