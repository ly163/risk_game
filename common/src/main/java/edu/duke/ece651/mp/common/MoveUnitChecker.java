package edu.duke.ece651.mp.common;

public class MoveUnitChecker implements RuleChecker {
    Class<MoveOrder> targetClass;


    public MoveUnitChecker() {
        this.targetClass = MoveOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        MoveOrder moveOrder = (MoveOrder) order;

        return moveOrder.units > 0;
    }
}
