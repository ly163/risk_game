package edu.duke.ece651.mp.common;

public class AttackUnitChecker implements RuleChecker {
    Class<AttackOrder> targetClass;

    public AttackUnitChecker() {
        this.targetClass = AttackOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        AttackOrder moveOrder = (AttackOrder) order;
        return moveOrder.units > 0;
    }
}
