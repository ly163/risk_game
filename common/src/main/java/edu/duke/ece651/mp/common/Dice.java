package edu.duke.ece651.mp.common;

public interface Dice {

  public int roll();
  
}
