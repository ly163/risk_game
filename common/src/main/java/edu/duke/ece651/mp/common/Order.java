package edu.duke.ece651.mp.common;

public interface Order {

  public void execute(WorldMap map);

  public void revoke();
  
}
