package edu.duke.ece651.mp.common;
/** This class describes the territory
 */
public class Territory {

  /** The Id of this territory
   */
  final int id;

  /** The name of this territory
   */
  final String name;
  // Player owner;

  /** The units on this territory
   */
  int units;

  /** The units which are going to attack this territory
   */
  int enemyUnits;

  public Territory(String name, int id) {
    this.name = name;
    this.id = id;
    this.units = 0;
    this.enemyUnits = 0;
  }

  public String getName() {
    return name;
  }
  
  public int getId() {
    return id;
  }

  public int getUnits() {
    return units;
  }

  public void setUnits(int units) {
    this.units = units;
  }

  public int getEnemyUnits() {
    return enemyUnits;
  }

  public void setEnemyUnits(int enemyUnits) {
    this.enemyUnits = enemyUnits;
  }

  public CombatResult combatSimulation(Player attacker, Player defender, Dice dice){
    if(enemyUnits == 0){
      return null;
    }
    while(units > 0 && enemyUnits > 0){
      int AP = dice.roll();
      int DP = dice.roll();
      if(AP > DP){
        units--;
      }
      else{
        enemyUnits--;
      }
    }
    if(enemyUnits == 0){
      return new CombatResult(attacker.getId(), defender.getId(), id, false);
    }
    else{
      units = enemyUnits;
      enemyUnits = 0;
      defender.removeTerritory(this);
      attacker.addTerritory(this);
      return new CombatResult(attacker.getId(), defender.getId(), id, true);
    }
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = prime * id + ((name == null) ? 0 : name.hashCode());
    return result;
  }

}
