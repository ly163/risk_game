package edu.duke.ece651.mp.common;

public class CombatResult {

  final int attackerId;

  final int defenderId;

  final int territoryId;
  
  final boolean succeed;

  public CombatResult(int attackerId, int defenderId, int territoryId, boolean succeed){
    this.attackerId = attackerId;
    this.defenderId = defenderId;
    this.territoryId = territoryId;
    this.succeed = succeed;
  }

  public int getTerritoryId(){
    return territoryId;
  }
  
  public int getAttackerId(){
    return attackerId;
  }

  public int getDefenderId(){
    return defenderId;
  }

  public boolean getSucceed(){
    return succeed;
  }
  
}
