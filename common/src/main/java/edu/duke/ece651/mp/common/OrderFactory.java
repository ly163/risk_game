package edu.duke.ece651.mp.common;

import java.util.HashMap;
import java.util.function.Function;

public class OrderFactory {

  final HashMap<String, Function<OrderParameter, Order>> createFns;

  private void setupCreateFns(){
    createFns.put("Move", (p) -> createMoveOrder(p));
    createFns.put("Attack", (p) -> createAttackOrder(p));
  }
  
  public OrderFactory(){
    createFns = new HashMap<String, Function<OrderParameter, Order>>();
    setupCreateFns();
  }

  private Order createMoveOrder(OrderParameter p){
    return new MoveOrder(p.getStartId(), p.getDestId(), p.getUnits());
  }

  private Order createAttackOrder(OrderParameter p){
    return new AttackOrder(p.getStartId(), p.getDestId(), p.getUnits());
  }

  public Order createOrder(OrderParameter p){
    if(createFns.containsKey(p.getOrderType())){
      return createFns.get(p.getOrderType()).apply(p);
    }
    return null;
  }
  
}
