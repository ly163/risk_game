package edu.duke.ece651.mp.common;

import java.util.Random;

public class Dice_20 implements Dice {

  final Random rand;

  public Dice_20(int seed){
    rand = new Random(seed);
  }
  
  @Override
  public int roll() {
    return rand.nextInt(20)+1;
  }
  
}
