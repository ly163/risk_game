package edu.duke.ece651.mp.common;

public class AttackAdjacentChecker implements RuleChecker {
    Class<AttackOrder> targetClass;

    public AttackAdjacentChecker() {
        this.targetClass = AttackOrder.class;
    }

    @Override
    public boolean check(Order order,WorldMap worldMap) {
        if (!targetClass.isInstance(order)) {
            return false;
        }
        AttackOrder moveOrder = (AttackOrder) order;

        Territory start = worldMap.findTerritory(moveOrder.startId);
        Territory end = worldMap.findTerritory(moveOrder.destId);
        return worldMap.isNeighbor(start, end);
    }
}
