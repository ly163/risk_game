package edu.duke.ece651.mp.common;

import java.util.HashSet;

/** This class is the player
 */
public class Player {

  /** Id of this player
   */
  final int id;

  /** Name of this player
   */
  final String name;

  /** holds all territories owned by this player
   */
  final HashSet<Territory> territories;

  public Player(int id, String name){
    this.id = id;
    this.name = name;
    territories = new HashSet<Territory>();
  }

  public int getId(){
    return id;
  }

  public String getName(){
    return name;
  }

  public HashSet<Territory> getTerritories(){
    return territories;
  }

  public void addTerritory(Territory t){
    territories.add(t);
  }

  public void removeTerritory(Territory t){
    territories.remove(t);
  }

  /** Checks if this player owns the territory
   * @param t is the Territory we want check
   * @return a boolean indicating if this player owns that territory
   */
  public boolean checkOwnership(Territory t){
    return territories.contains(t);
  }
  
}
