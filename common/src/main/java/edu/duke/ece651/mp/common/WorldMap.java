package edu.duke.ece651.mp.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class WorldMap {

  /** All territories in this map
   */
  final ArrayList<Territory> territories;

  /** the adjacency information for this map
   */
  boolean[][] adjacencyMatrix;

  /** All players in this map
   */
  final ArrayList<Player> players;

  public WorldMap(ArrayList<Territory> territories, HashMap<Territory, ArrayList<Territory>> adjacencies, ArrayList<Player> players){
    this.territories = territories;
    int size = territories.size();
    adjacencyMatrix = new boolean[size][size];
    for(int i = 0; i < size; i++){
      for(int j = 0; j < size; j++){
        adjacencyMatrix[i][j] = false;
      }
    }
    for(Territory t : territories){
      for(Territory n : adjacencies.get(t)){
        adjacencyMatrix[t.getId()][n.getId()] = true;
      }
    }
    this.players = players;
  }

  public ArrayList<Territory> getTerritories(){
    return territories;
  } 
  
  /** Get the neighbors of a Territory
   * @param t is a Territory
   * @return an ArrayList of Territory with all of t's neighbors
   */
  public ArrayList<Territory> getNeighbors(Territory t){
    ArrayList<Territory> list = new ArrayList<Territory>();
    int size = territories.size();
    for(int i = 0; i < size; i++){
      if(adjacencyMatrix[t.getId()][i]){
        list.add(findTerritory(i));
      }
    }
    return list;
  }

  public ArrayList<Player> getPlayers(){
    return players;
  }

  /** Check if the given player owns the given Territory
   * @param p is the Player
   * @param t is the Territory
   * @return a boolean indicating if p owns t
   */
  public boolean checkOwnership(Player p, Territory t){
    return p.checkOwnership(t);
  }

  /** Checks if two territories are connected across one player's territories.
   * @param start is the Territory we begin with
   * @param end is the target Territory
   * @param p is the Player whose territories the final path can go through
   * @return a boolean indicating if a path exists
   */
  public boolean isConnected(Territory start, Territory end, Player p){
    if(start == end){
      return true;
    }
    HashSet<Territory> visited = new HashSet<Territory>();
    Queue<Territory> q = new LinkedList<Territory>();
    visited.add(start);
    q.add(start);
    while(!q.isEmpty()){
      Territory curr = q.poll();
      if(curr == end){
        return true;
      }
      for(Territory t : getNeighbors(curr)){
        if(checkOwnership(p, t)){
          if(!visited.contains(t)){
            q.add(t);
            visited.add(t);
          }
        }
      }
    }
    return false;   
  }

  /** Check if two territories are neighbors on the map
   * @param start is the territory to begin with
   * @param end is the target Territory
   * @return a boolean indicating if they are neighbors
   */
  public boolean isNeighbor(Territory start, Territory end){
    return getNeighbors(start).contains(end);
  }

  public Territory findTerritory(int Id){
    for(Territory t : territories){
      if(t.getId() == Id){
        return t;
      }
    }
    return null;
  }

  public Player getOwner(Territory t){
    for(Player p : players){
      if(p.checkOwnership(t)){
        return p;
      }
    }
    return null;
  }

  public ArrayList<CombatResult> combatSimulation(Dice dice){
    ArrayList<CombatResult> results = new ArrayList<CombatResult>();
    Player p1 = players.get(0);
    Player p2 = players.get(1);
    for(Territory t : p1.getTerritories()){
      CombatResult result = t.combatSimulation(p2, p1, dice);
      if(result != null){
        results.add(result);
      }
    }
    for(Territory t : p2.getTerritories()){
      CombatResult result = t.combatSimulation(p1, p2, dice);
      if(result != null){
        results.add(result);
      }
    }
    return results;
  }
  
}
