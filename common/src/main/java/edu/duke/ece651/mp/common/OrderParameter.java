package edu.duke.ece651.mp.common;

public class OrderParameter {

  final int startId;

  final int destId;

  final int units;

  final String orderType;

  public OrderParameter(int startId, int destId, int units, String orderType){
    this.startId = startId;
    this.destId = destId;
    this.units = units;
    this.orderType = orderType;
  }

  public int getStartId(){
    return startId;
  }

  public int getDestId(){
    return destId;
  }

  public int getUnits(){
    return units;
  }

  public String getOrderType(){
    return orderType;
  }
  
}
