package edu.duke.ece651.mp.common;

public class MoveOrder implements Order {

  final int startId;

  final int destId;

  final int units;

  private WorldMap map = null;

  public MoveOrder(int startId, int destId, int units){
    this.startId = startId;
    this.destId = destId;
    this.units = units;
  }
  
  @Override
  public void execute(WorldMap MAP) {
    if(map == null){
      map = MAP;
      Territory start = map.findTerritory(startId);
      Territory dest = map.findTerritory(destId);
      start.setUnits(start.getUnits() - units);
      dest.setUnits(dest.getUnits() + units);
    }
  }

  @Override
  public void revoke() {
    Territory start = map.findTerritory(startId);
    Territory dest = map.findTerritory(destId);
    start.setUnits(start.getUnits() + units);
    dest.setUnits(dest.getUnits() - units);  
  }

}
