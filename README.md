ECE 651: Project: RISC Game
======================================

![pipeline](https://gitlab.oit.duke.edu/hx70/ece-651-risk/badges/master/pipeline.svg)

![coverage](https://gitlab.oit.duke.edu/hx70/ece-651-risk/badges/master/coverage.svg?job=test)

At the moment, there is only minimal working system.

## Coverage
[Detailed Coverage](https://hx70.pages.oit.duke.edu/ece-651-risk/dashboard.html)

## UML Diagram
[Lucid Chart](https://lucid.app/lucidchart/a5707228-bf44-469d-b949-db186c625743/edit?invitationId=inv_a24d8eae-63b9-4051-8cc8-8c3112b055fb)

## Project Management
[Google Docs](https://docs.google.com/spreadsheets/d/1Boej2zqMQXycWE_waSEswEDvYAIN_V8wbO__EQz99PY/edit?usp=sharing)