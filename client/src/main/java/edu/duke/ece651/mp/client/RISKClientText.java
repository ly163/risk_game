package edu.duke.ece651.mp.client;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import edu.duke.ece651.mp.common.AttackOrder;
import edu.duke.ece651.mp.common.CombatResult;
import edu.duke.ece651.mp.common.MoveOrder;
import edu.duke.ece651.mp.common.Order;
import edu.duke.ece651.mp.common.Territory;
import edu.duke.ece651.mp.common.WorldMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.ArrayList;

public class RISKClientText {

    final PrintStream out;

    final BufferedReader inputReader;

    final Socket server;

    public RISKClientText(PrintStream out, BufferedReader inputReader, Socket server) {
        this.out = out;
        this.inputReader = inputReader;
        this.server = server;
    }

    public String displayMap(WorldMap map) {
        StringBuilder display = new StringBuilder();
        for (Territory t : map.getTerritories()) {
            display.append(t.getName());
            display.append("\n");
        }
        return display.toString();
    }

    public String receiveJSON() throws IOException {
        InputStream in = server.getInputStream();
        ObjectInputStream i = new ObjectInputStream(in);
        String line = null;
        try {
            line = (String) i.readObject();
        } catch (Exception e) {
        }
        return line;
    }

    public WorldMap receiveWorldMap() throws IOException {
        String line = receiveJSON();
        Gson gson = new Gson();
        Type type = new TypeToken<WorldMap>() {
        }.getType();
        WorldMap map = gson.fromJson(line, type);
        return map;
    }

    public Order readOrder() throws IOException {
        out.println("Order Example:");
        out.println("Move Order: M-1-4-12");
        out.println("Attack Order: A-1-2-52");
        out.println("Operation Finish: Done");
        out.println();

        while (true) {
            String cmd = inputReader.readLine();
            String s = cmd.toUpperCase();
            String[] slice = s.split("-");
            if (slice.length > 4) {
                out.println("Invalid Order: " + cmd);
                continue;
            }
            if (s.equals("DONE")) {
                out.println("Done!");
                return null;
            }
            char type = s.charAt(0);
            try {
                if ((type == 'M' || type == 'A')) {
                    int startId = Integer.parseInt(slice[1]);
                    int destId = Integer.parseInt(slice[2]);
                    int units = Integer.parseInt(slice[3]);
                    if (type == 'M') {
                        return new MoveOrder(startId, destId, units);
                    } else {
                        return new AttackOrder(startId, destId, units);
                    }
                } else {
                    out.println("Invalid Order: " + cmd);
                }
            } catch (Exception e) {
                out.println("Invalid Order: " + cmd);
            }

        }
    }

    private boolean isDigital(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private void displayStatus() {

    }

    private WorldMap displayMap() {
        return null;
    }

    private ArrayList<CombatResult> displayCombatResults() {
        return new ArrayList<>();
    }
}
