package edu.duke.ece651.mp.client;

import com.google.gson.Gson;
import edu.duke.ece651.mp.common.AttackOrder;
import edu.duke.ece651.mp.common.MapFactory;
import edu.duke.ece651.mp.common.MoveOrder;
import edu.duke.ece651.mp.common.Order;
import edu.duke.ece651.mp.common.Territory;
import edu.duke.ece651.mp.common.WorldMap;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class RISKClientTextTest {

    private void sendJSON(Object obj) throws IOException {
        ServerSocket ss = new ServerSocket(6666);
        Socket client = ss.accept();
        Gson gson = new Gson();
        String toSend = gson.toJson(obj);
        OutputStream out = client.getOutputStream();
        ObjectOutputStream o = new ObjectOutputStream(out);
        o.writeObject(toSend);
        out.flush();
    }

    @Test
    public void test_displayMap() {
        Territory test = new Territory("test", 0);
        ArrayList<Territory> tests = new ArrayList<Territory>();
        tests.add(test);
        MapFactory f = new MapFactory();
        WorldMap map = f.createBasicMap();
        RISKClientText client = new RISKClientText(null, null, null);
        assertEquals("t1\nt2\nt3\nt4\nt5\nt6\n", client.displayMap(map));
    }

    @Test
    public void test_receiveWorldMap() throws Exception {
        MapFactory f = new MapFactory();
        WorldMap map = f.createBasicMap();
        Thread th = new Thread() {
            @Override()
            public void run() {
                try {
                    sendJSON(map);
                } catch (Exception e) {
                }
            }
        };
        th.start();
        Thread.sleep(100);
        Socket server = new Socket("127.0.0.1", 6666);
        RISKClientText client = new RISKClientText(null, null, server);
        //assertEquals("", line);
        assertEquals("t1\nt2\nt3\nt4\nt5\nt6\n", client.displayMap(client.receiveWorldMap()));
    }

    @Test
    public void test_read_order() {
        StringReader stringReader = new StringReader("A-1-2-1\nM-1-3-19\nA-1-2-14\nDoNe\n");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);
        RISKClientText clientText = new RISKClientText(out, new BufferedReader(stringReader), null);
        try {
            Order order = clientText.readOrder();
            assertTrue(reflectEquals(new AttackOrder(1, 2, 1), order));
        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
        try {
            Order order = clientText.readOrder();
            assertTrue(reflectEquals(new MoveOrder(1, 3, 19), order));
        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            fail();
        }
        try {
            Order order = clientText.readOrder();
            assertFalse(reflectEquals(new MoveOrder(1, 3, 15), order));
        } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
            fail();
        }
        try {
            Order order = clientText.readOrder();
            assertNull(order);
        } catch (IOException e) {
            fail();
        }
    }

    private boolean reflectEquals(Object o1, Object o2) throws NoSuchFieldException, IllegalAccessException {
        if (o1.getClass() != o2.getClass()) {
            return false;
        }
        Class<?> cls = o1.getClass();
        for (Field field : cls.getDeclaredFields()) {
            String name = field.getName();
            Field declaredField = cls.getDeclaredField(name);
            declaredField.setAccessible(true);
            if (!Objects.deepEquals(declaredField.get(o1),declaredField.get(o2))){
                return false;
            }
        }
        return true;
    }
}
